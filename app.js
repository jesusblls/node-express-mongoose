var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var mongoose = require('mongoose');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(express.static('public'));

// middleware para habilitar CORS
app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  next();
});

mongoose.connect('mongodb://192.168.99.100/blog');



var router = require('./router');
app.use('/', router);

var server = app.listen(3005, function() {
  console.log("Listening on port %s...", server.address().port);
});
